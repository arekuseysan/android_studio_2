package ru.arekuseysan.lesson2503

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidUnit4
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert.*

@RunWith(AndroidUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() [
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("ru.arekuseysan.lesson2503", appContext.packageName)
    ]
}