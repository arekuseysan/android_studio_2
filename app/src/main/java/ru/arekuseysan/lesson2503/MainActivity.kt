package ru.arekuseysan.lesson2503

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import java.util.Arrays


class MainActivity : AppCompatActivity(), View.OnClickListener {
	private lateinit var editTextId: EditText
	private lateinit var editTextName: EditText
	private lateinit var editTextEmail: EditText

	private lateinit var buttonInsert: Button
	private lateinit var buttonGetStudents: Button
	private lateinit var buttonGetStudentById: Button
	private lateinit var buttonUpdateStudent: Button
	private lateinit var buttonDeleteStudents: Button
	private lateinit var buttonDeleteStudentById: Button

	private lateinit var dbHelper: DBHelper

	private val validEmails = arrayOf("@mail.ru", "@bk.ru", "@gmail.com", "@ya.ru", "@yandex.ru")
	private var sb = StringBuilder()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		editTextId = findViewById<View>(R.id.editTextId) as EditText
		editTextName = findViewById<View>(R.id.editTextName) as EditText
		editTextEmail = findViewById<View>(R.id.editTextEmail) as EditText

		buttonInsert = findViewById(R.id.buttonInsert);
		buttonGetStudents = findViewById(R.id.buttonGetStudents);
		buttonGetStudentById = findViewById(R.id.buttonGetStudentById);
		buttonUpdateStudent = findViewById(R.id.buttonUpdateStudent);
		buttonDeleteStudents = findViewById(R.id.buttonDeleteStudents);
		buttonDeleteStudentById = findViewById(R.id.buttonDeleteStudentById);

		dbHelper = DBHelper(this)

		buttonInsert.setOnClickListener(this)
		buttonGetStudents.setOnClickListener(this)
		buttonGetStudentById.setOnClickListener(this)
		buttonUpdateStudent.setOnClickListener(this)
		buttonDeleteStudents.setOnClickListener(this)
		buttonDeleteStudentById.setOnClickListener(this)
	}

	@SuppressLint("Range")
	override fun onClick(v: View) {
		when (v.id) {
			R.id.buttonInsert -> {
				val name = editTextName.text.toString()
				val email = editTextEmail.text.toString()
				val validEmail = this.checkEmail(email)

				if (!validEmail) {
					showMessage("Insert student:", sb.toString());
					return
				}

				dbHelper.insertStudent(name, email)
				sb.append("Successful inserted student")
				showMessage("Insert student:", sb.toString());
			}

			R.id.buttonGetStudents -> {
				sb = StringBuilder()
				val cursor: Cursor = dbHelper.getStudents()
				while (cursor.moveToNext()) {
					sb.append("ID: " + cursor.getString(cursor.getColumnIndex(DBHelper.KEY_ID)) + "\n")
					sb.append("Name: " + cursor.getString(cursor.getColumnIndex(DBHelper.KEY_NAME)) + "\n")
					sb.append("Email: " + cursor.getString(cursor.getColumnIndex(DBHelper.KEY_EMAIL)) + "\n\n")
				}
				showMessage("All students:", sb.toString());
			}

			R.id.buttonGetStudentById -> {
				val id = editTextId.text.toString()
				val students = this.getStudentArrayList()
				var student: Student? = null
				sb = StringBuilder()

				for (st: Student in students) {
					if (st.id == id) {
						student = st
					}
				}

				if (student != null) {
					sb.append("ID: " + student.id + "\n")
					sb.append("Name: " + student.name + "\n")
					sb.append("Email: " + student.email + "\n\n")
				} else {
					sb.append("Student with $id does not find in database")
				}
				showMessage("Student by ID:", sb.toString());
			}

			R.id.buttonUpdateStudent -> {
				val students = this.getStudentArrayList()
				val id = editTextId.text.toString()
				val name = editTextName.text.toString()
				val email = editTextEmail.text.toString()
				var student: Student? = null
				var isIdExists = false
				sb = StringBuilder()
				val validEmail = checkEmail(email)

				if (!validEmail) {
					showMessage("Update student:", sb.toString());
					return
				}

				for (st in students) {
					if (st.id == id) {
						student = st
						isIdExists = true
					}
				}
				if (isIdExists) {
					if (student!!.name == name && student.email == email) {
						sb.append("No change data! Break operation")
						showMessage("Update student:", sb.toString())
						return
					}
					sb.append("ID: $id\n\n")
					sb.append("Old name: " + student.name + "\n")
					sb.append("Old email: " + student.email + "\n\n")
					dbHelper.updateStudent(id, name, email)
					sb.append("New name: $name\n")
					sb.append("New email: $email\n")
				} else {
					sb.append("Student with $id does not find in database")
				}
				showMessage("Update student:", sb.toString());
			}

			R.id.buttonDeleteStudents -> {
				val sb = StringBuilder()
				dbHelper.deleteAllStudents()
				sb.append("Successful delete all users")
				showMessage("Delete users:", sb.toString())
			}

			R.id.buttonDeleteStudentById -> {
				val id = editTextId.text.toString()
				val result = dbHelper.deleteStudentById(id)
				val sb = StringBuilder()
				if (result == 1) {
					sb.append("Successful delete user with ID: $id")
				} else {
					sb.append("Failed to delete user with ID: $id")
				}
				showMessage("Delete users:", sb.toString())
			}
		}
		dbHelper.close()
	}

	private fun showMessage(title: String, msg: String) {
		val builder = AlertDialog.Builder(this)
		builder.create()
		builder.setCancelable(true)
		builder.setTitle(title)
		builder.setMessage(msg)
		builder.show()
	}

	@SuppressLint("Range")
	fun getStudentArrayList(): ArrayList<Student> {
		val cursor: Cursor = dbHelper.getStudents()
		val students = ArrayList<Student>()
		while (cursor.moveToNext()) {
			students.add(
				Student(
					cursor.getString(cursor.getColumnIndex(DBHelper.KEY_ID)),
					cursor.getString(cursor.getColumnIndex(DBHelper.KEY_NAME)),
					cursor.getString(cursor.getColumnIndex(DBHelper.KEY_EMAIL))
				)
			)
		}
		return students
	}

	private fun checkEmail(email: String): Boolean {
		val dog = email.indexOf('@')
		sb = StringBuilder()
		if (dog == -1)
		{
			sb.append("Incorrect email address. \n\nEmail address must contain '@'")
		} else {
			val str = email.substring(dog)
			if (!validEmails.contains(str)) {
				sb.append("Incorrect email address. \n\nAllowed E-Mail domains: " + validEmails.contentToString().replace("[", "").replace("]", ""))
			} else {
				return true
			}
		}
		return false
	}
}

