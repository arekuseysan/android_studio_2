package ru.arekuseysan.lesson2503

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, 2) {
	override fun onCreate(db: SQLiteDatabase) {
		db.execSQL(
			"create table if not exists " + TABLE_NAME + "(id integer primary key autoincrement,"
					+ KEY_NAME + " text," + KEY_EMAIL + " text)"
		)
	}

	override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
		db.execSQL("drop table if exists " + TABLE_NAME)
		onCreate(db)
	}

	fun insertStudent(name: String, email: String): Boolean {
		val database: SQLiteDatabase = this.writableDatabase
		val contentValues = ContentValues()
		contentValues.put(KEY_NAME, name)
		contentValues.put(KEY_EMAIL, email)
		val v: Long = database.insert(TABLE_NAME, null, contentValues)
		return v != -1L;
	}

	fun getStudents(): Cursor {
		val database: SQLiteDatabase = this.writableDatabase
		val query = "select * from $TABLE_NAME"
		return database.rawQuery(query, null)
	}

	fun getStudentById(id: String): Cursor {
		val database: SQLiteDatabase = this.writableDatabase
		val query = "select * from $TABLE_NAME where id = '$id'"
		return database.rawQuery(query, null)
	}

	fun updateStudent(id: String, name: String, email: String) {
		val database: SQLiteDatabase = this.writableDatabase
		val contentValues = ContentValues()
		contentValues.put(KEY_NAME, name)
		contentValues.put(KEY_EMAIL, email)
		database.update(TABLE_NAME, contentValues, "id=?", arrayOf(id))
	}

	fun deleteAllStudents() {
		val database: SQLiteDatabase = this.writableDatabase
		database.delete(TABLE_NAME, null, null)
	}

	fun deleteStudentById(id: String): Int {
		val database: SQLiteDatabase = this.writableDatabase
		return database.delete(TABLE_NAME, "id=?", arrayOf(id))
	}

	companion object {
		const val DATABASE_NAME = "MyDB1"
		const val TABLE_NAME = "students"
		const val KEY_ID = "id"
		const val KEY_NAME = "name"
		const val KEY_EMAIL = "email"
	}
}

